
import {getItems} from '../data'

let initialState = {
    users: []
}
const usersReducer = (state = initialState, action) => {

    switch (action.type) {
        
        case "SET_USERS":
            
            return {
                ...state, users: action.users
            }
                default:
                    return state;
    }

}
const setUsers = (users) => {
    return {
        type: "SET_USERS",
        users
    }
}
export  const setUsersThunk = () => {
    return async (dispatch) => {
    const data = await getItems()
    dispatch(setUsers(data))
 }
   
}

export default usersReducer