
import { getProfile} from '../data'
let initialState = {
    profile: {}
}
const profileReducer = (state = initialState, action) => {

    switch (action.type) {
        
        case "SET_PROFILE":
            
            return {
                ...state, profile: action.profile
            }
                default:
                    return state;
    }

}

const setProfile = (profile) => {
    return {
        type: "SET_PROFILE",
        profile
    }
}

export  const setProfileThunk = (id) => {
    return async (dispatch) => {
    const data = await getProfile(id)
    return dispatch(setProfile(data))
 }
}
export default profileReducer