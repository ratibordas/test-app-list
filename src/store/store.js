import { createStore, applyMiddleware,combineReducers} from "redux";
import usersReducer from '../reducer/users-reducer'
import thunkMiddleware from 'redux-thunk'
import profileReducer from "../reducer/profile-reducer";



let reducers = combineReducers({
    usersPage: usersReducer,
    profilePage: profileReducer
})

export const store = createStore(reducers,applyMiddleware(thunkMiddleware))


