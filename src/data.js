import Axios from 'axios'


const customAxios = Axios.create({
    baseURL: "https://jsonplaceholder.typicode.com/"
  });
  export const getItems = () => {
    return customAxios.get(`users`).then(response => { 
      
       return response.data
    })
    
    }
    export const getProfile = (id) => {
      return customAxios.get(`users/${id}`).then(response => { 
         return response.data
      })
      
      }