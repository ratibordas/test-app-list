import React, { useEffect } from 'react'
import { setProfileThunk } from '../reducer/profile-reducer'
import { connect } from 'react-redux';
import {  withRouter } from 'react-router-dom';
import { compose } from 'redux';




 const Profile = (props) => {
     useEffect(() => { 
        let id = props.match.params.id;
        props.setProfileThunk(Number.parseInt(id));
     },[props.match.params.id])

     return (
       <div>
<p>Name: {props.profile.name} </p>    
<p> Phone: {props.profile.phone}</p>
       </div>
     )
 }
const mapStateToProps = (state) => {
    return {
        profile: state.profilePage.profile
    }
}
export default compose(connect(mapStateToProps, { setProfileThunk }), withRouter)(Profile);

