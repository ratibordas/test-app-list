import React,{useState} from 'react'
import { Link } from 'react-router-dom';
import { InputGroup, Label } from '@blueprintjs/core';




const Users = (props) => {
    const [item, setItem] = useState("")
    const addSearch = (e) => {
        const item = e.target.value;
        setItem(item);  
      }
      const regexp = new RegExp(item, 'i');
    return (
        <section>
         <nav >
            <Label>
                Search  <InputGroup placeholder="text here..."
                    type="text"  onChange={addSearch} value={item} />
            </Label>
        </nav>
        <ul>
            {
                props.users.filter(u  => regexp.test(u.name)).map(user => {
                    
                    return <li key={user.id}>
                        <Link to={"/profile/" + user.id}>
                            {user.name} </Link>
                    </li>
                })
            }
        </ul>
        </section>
    )
}

export default Users