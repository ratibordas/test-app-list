import React from 'react'



const Items = ({ items }) => 
  Array.isArray(items) && items.length
    ? <div>
        {items.map(n => (
         <div >
            <h2 >{n.title}</h2>
            <p  >{n.text}</p>
            <Items items={n.items} />
          </div>
        ))}
      </div>
    : null;


    export default Items