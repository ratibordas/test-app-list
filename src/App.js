
import './App.css';
import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { setUsersThunk } from './reducer/users-reducer'
import Users from './Users/Users';
import { Switch, Route, Link } from 'react-router-dom'
import Profile from './Profile/Profile'
import Items from './Items/Items'

const App = (props) => {

  const items = [
    {
      title: 'Elements',
      items: [
        {
          title: '1',
          text: 'This is First one.',
          items: [
            {
              title: '1:1',
              text: 'This is First of the First one.',
              items: [
                { title: '1:1:1', text: 'This is First of the First of the First one.' },
                { title: '1:1:2', text: 'This is Second of the First of the First one.', }
              ]
            },
            {
              title: '1:2',
              text: 'This is Second of the First one.',
              items: [
                { title: '1:2:1', text: 'This is First of the Second of the First one.' },
                { title: '1:2:2', text: 'This is Second of the Second of the First one.' },
                { title: '1:2:3', text: 'This is Third of the Second of the First one.' },
              ]
            },
          ]
        },
        {
          title: '2',
          text: 'This is Second one.',
          items: [
            {
              title: '2:1',
              text: 'This is First of the Second one.',
              items: [
                { title: '2:1:1', text: 'This is First of the First of the Second one.' },
                { title: '2:1:2', text: 'This is Second of the First of the Second one.' }
              ]
            }
          ]
        },
        {
          title: '3',
          text: 'This is Third one.'
        }
      ]
    }
  ];
  useEffect(() => {
    props.setUsersThunk()
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <nav className="navbar">
        <Link className="task2" to="/items" >Items</Link>
        <br/>
        <Link className="task1" to="/" >Users</Link>
        </nav>
     
        <Switch>
          <Route exact path="/" >
            <Users users={props.users.users} />

          </Route>
          <Route path="/profile/:id"  >
            <Profile />
            
          </Route >
          <Route path="/items">
          <Items  items={items}/>
          </Route>
        </Switch>

      </header>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    users: state.usersPage

  }
}
export default connect(mapStateToProps, { setUsersThunk })(App);
